import 'package:first_app/quiz.dart';
import 'package:first_app/result.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  static const List<Map<String, Object>> questions = [
    {
      "question": "What's your name?",
      "answer": [
        {'text': 'Zeze', 'score': 3},
        {'text': 'John', 'score': 2},
        {'text': 'Ball', 'score': 1},
      ],
    },
    {
      "question": "How old are you?",
      "answer": [
        {'text': '12', 'score': 3},
        {'text': '11', 'score': 2},
      ],
    },
    {
      "question": "How are you?",
      "answer": [
        {'text': 'Cool', 'score': 3},
        {'text': 'Soso', 'score': 0},
      ],
    },
  ];
  int i = 0;
  int _totalScore = 0;

  void _resetQuiz() {
    setState(() {
      i = 0;
      _totalScore = 0;
    });
  }

  void answerQuestion(int score) {
    setState(() {
      if (i < questions.length) {
        i++;
        _totalScore += score;
      } else {
        print("Index out of range");
      }
    });
    print("Answer Choosen!");
    print(_totalScore);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("My First App"),
        ),
        body: i < questions.length
            ? Quiz(
                answerQuestion: answerQuestion,
                questions: questions,
                index: i,
              )
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}
