import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final VoidCallback resetQuiz;

  String get resultPhrase {
    var resultText = 'You did it! $resultScore';
    if (resultScore < 6) {
      resultText = 'You are OK $resultScore';
    } else {}
    return resultText;
  }

  Result(this.resultScore, this.resetQuiz);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Center(
            child: Text(
              resultPhrase,
              style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
            ),
          ),
          TextButton(
            onPressed: resetQuiz,
            child: Text("Restart"),
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              backgroundColor: MaterialStateProperty.all<Color>(Colors.amber),
            ),
          )
        ],
      ),
    );
  }
}
